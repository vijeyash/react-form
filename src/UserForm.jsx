import React, { useState } from "react";
import { Form, Button, Alert } from "react-bootstrap";

const UserForm = () => {
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [emailerror, setemailerror] = useState("");
  const [passworderror, setpassworderror] = useState("");
  const date = new Date();

  const handleSubmit = (event) => {
    event.preventDefault();
    var emailValid = false;
    if (email.length === 0) {
      setemailerror("email is required");
    } else if (email.length < 12) {
      setemailerror("email cannot be less than 12 characters in total");
    } else if (email.indexOf(" ") >= 0) {
      setemailerror("email cannot contains spaces");
    } else {
      setemailerror("");
      emailValid = true;
    }
    var passwordValid = false;
    if (password.length === 0) {
      setpassworderror("password is required");
    } else if (password.length < 6) {
      setpassworderror("password cannot be less than 12 characters in total");
    } else if (password.indexOf(" ") >= 0) {
      setpassworderror("password cannot contains spaces");
    } else {
      setpassworderror("");
      passwordValid = true;
    }
    if (emailValid && passwordValid) {
      alert(
        `Thank you for entering your email: ${email} and password: ${password}. 
         Have a nice day ${date} `
      );
    }
  };
  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(event) => setemail(event.target.value)}
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>
        {emailerror.length > 0 && <Alert variant="danger">{emailerror}</Alert>}

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(event) => setpassword(event.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Check me out" />
        </Form.Group>
        {passworderror.length > 0 && (
          <Alert variant="danger">{passworderror}</Alert>
        )}
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </div>
  );
};

export default UserForm;
